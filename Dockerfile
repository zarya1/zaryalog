FROM golang:1.14-alpine AS build
WORKDIR /go/src/zaryalog
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -tags "netgo" -installsuffix netgo -o /go/bin/zaryalog ./cmd/zaryalog

FROM scratch
COPY --from=build /go/bin/zaryalog /bin/zaryalog
ADD ./ghp/grpc-health-probe /bin/grpc-health-probe
ENTRYPOINT ["/bin/zaryalog"]
